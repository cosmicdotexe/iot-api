var key = "vDZt7TTU149iVH6xlrNbdt3aEKT7Gqpe";
var resultnumber = 0;
var locationarray = new Array();
var markernumber = new Array();
var contentString = new Array();
var inputcurrent = "0";
var infowindow = new google.maps.InfoWindow();
var inputITD = "";
var inputDTI = "";
var marker = "";
var i = "";
var page = 0;
init();

function init() {
  getJSON();
  var drop = document.getElementById('drop');
  var field = document.getElementById('ValueField');

  field.addEventListener('keyup', function (e) {
    if (e.keyCode == 32 || e.keyCode == 13) {
      UpdateInput();
    }
  });
}

function getJSON() {
  var url = "https://api.shodan.io/shodan/host/search?key=" + key + "&query=" + inputcurrent + "&page=" + page;
  console.log(url);
  $.getJSON(url)
    .done(successHandler)
    .fail(function (jqxhr, status, error) {
      alert(error);
      document.getElementById('loader').style = "opacity: 0;";
    });
};

function successHandler(data) {
  if (data.matches.length == 0) {
    resultnumber = 0;
    document.getElementById('resultnumber').innerHTML = "Total Results : '" + resultnumber + "'";
  } else {
    for (var i = 0; i < data.matches.length; i++) {
      j = i + 100 * page;
      locationarray[i] = { lat: data.matches[i].location.latitude, lng: data.matches[i].location.longitude };
      contentString[i] = "https://www.shodan.io/host/" + data.matches[i].ip_str + "<br />ID: " + j;
    }
    resultnumber = data.total;
    maxpages = Math.ceil(resultnumber/100)-1;
    resultstring = resultnumber.toString();
    resultlenght = resultnumber.toString().length;
    if (resultlenght >= 0 && resultlenght <= 3) {
      resultnumber2 = resultstring;
    }
    if (resultlenght >= 3 && resultlenght <= 6) {
      resultnumber2 = resultstring.substring(resultlenght - 6, resultlenght - 3) + " " + resultstring.substring(resultlenght - 3, resultlenght);
    }
    if (resultlenght >= 7 && resultlenght <= 9) {
      resultnumber2 = resultstring.substring(resultlenght - 9, resultlenght - 6) + " " + resultstring.substring(resultlenght - 6, resultlenght - 3) + " " + resultstring.substring(resultlenght - 3, resultlenght);
    }


    for (var i = 0; i < data.matches.length; i++) {
      if (data.matches[i].location.city == null) {
        var city = "Unknown";
      } else {
        var city = data.matches[i].location.city;
      }
      j = i + 100 * page;
      document.getElementById('listresults').innerHTML += "<tr><td>" + j + "</td>" + "<td>" + city + "</td><td><a href='https://www.shodan.io/host/" + data.matches[i].ip_str + "'>" + data.matches[i].ip_str + "</td></a><td><a href='http://" + data.matches[i].ip_str + "/'>" + data.matches[i].data.substr(0, 12) + "</td></tr>";
      document.getElementById('resultnumber').innerHTML = "Total Results : '" + resultnumber2 + "'<button type='button' class='btn btn-info btn-sm pull-right' data-toggle='modal' data-target='#myModal'>DNS Resolve / Reverse</button> <button type='button' class='btn btn-info btn-sm pull-right' id='next'>Next Page</button> <button type='button' class='btn btn-info btn-sm pull-right' id='prev'>Previous Page</button>";
      document.getElementById("next").addEventListener("click", pageup);
      document.getElementById("prev").addEventListener("click", pagedown);
    }
  }
  createMap();
  updateMap();
}

function pagedown() {
  if(page >= 1){
    page -= 1;
    UpdateInput();
  }else{
    alert("already on the first page !");    
  }
}

function pageup() {
  if (page < maxpages){
    page += 1;
    UpdateInput();
  }else{
    alert("already on the last page !");        
  }
}

function createMap(){
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 4,
    center: locationarray[0]
  });
  for (i = 0; i < 98; i++) {
    marker = new google.maps.Marker({
      position: new google.maps.LatLng(locationarray[i]),
      map: map
    });
}

function updateMap() {
    google.maps.event.addListener(marker, 'click', (function (marker, i) {
      return function () {
        infowindow.setContent(contentString[i]);
        infowindow.open(map, marker);
      }
    })(marker, i));
  }
  document.getElementById('loader').style = "opacity: 0;";
}

function openInNewTab(url) {
  var win = window.open(url, '_blank');
  win.focus();
}

function UpdateInput() {
  document.getElementById('loader').style = "opacity: 1";
  inputcurrent = document.getElementById('drop').value + document.getElementById('ValueField').value;
  document.getElementById('listresults').innerHTML = "";
  getJSON();
  document.getElementById('ValueField').placeholder = inputcurrent;
}

function IpToDomain() {
  inputITD = document.getElementById('ValueIptoDomain').value;
  var urlITD = "https://api.shodan.io/dns/resolve?ips=" + inputITD + "&key=" + key;
  $.getJSON(urlITD)
    .done(successHandlerITD)
    .fail(function (jqxhr, status, error) {
      alert(error);
    });
}

function successHandlerITD(data) {
  var outputITD = data[inputITD];
  document.getElementById('ValueDomaintoIp').value = outputITD;
}

function DomainToIp() {
  inputDTI = document.getElementById('ValueDomaintoIp').value;
  var urlDTI = "https://api.shodan.io/dns/resolve?hostnames=" + inputDTI + "&key=" + key;
  $.getJSON(urlDTI)
    .done(successHandlerDTI)
    .fail(function (jqxhr, status, error) {
      alert(error);
    });
}

function successHandlerDTI(data) {
  var outputDTI = data[inputDTI];
  document.getElementById('ValueIptoDomain').value = outputDTI;
}